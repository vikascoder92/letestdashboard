import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../admin/service/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: any;
  password: any;
  constructor(private authenticationService: AuthenticationService, private _router: Router) {
    if (this.authenticationService.loggedIn) {
      this._router.navigate(['admin/dashboard']);
    }
  }
  ngOnInit() {
  }

  login(): void {
    if (this.username != '' && this.password != '') {
      if (this.authenticationService.login(this.username, this.password)) {
        this._router.navigate(["admin"]);
      }
      else
        alert("Wrong username or password");
    }
  }

  signup() {
    this._router.navigateByUrl('signup');
  }
}
