import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HttpClientModule } from  '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from './admin/service/authentication.service';
import { AuthGuard } from './admin/service/auth.guard';
import { AdminModule } from './admin/admin.module';
import { CommonModule } from '@angular/common';
import { SignupComponent } from './signup/signup.component';
import { ErrorComponent } from './error/error.component';
import { DashComponent } from './dash/dash.component';
import { GetCountService } from './services/get-count.service';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    SignupComponent,
    ErrorComponent,
    DashComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    AdminModule,
    HttpClientModule,


    
  ],
  providers: [AuthenticationService,AuthGuard,GetCountService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
