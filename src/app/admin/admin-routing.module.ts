import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthGuard } from './service/auth.guard';
import { ContactComponent } from './adminComponent/contact/contact.component';
import { DashboardComponent } from './adminComponent/dashboard/dashboard.component';
import { ProfileComponent } from './adminComponent/profile/profile.component';
import { UserDetailsComponent } from './adminComponent/user-details/user-details.component';
import { StatesComponent } from './adminComponent/states/states.component';
import { ErrorComponent } from './adminComponent/error/error.component';
import { FormComponent } from './adminComponent/form/form.component';
const routes: Routes = [
  {
    path: '', component: AdminComponent, canActivate: [AuthGuard],
    children: [

      { path: '', component: DashboardComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'contactDetails', component: ContactComponent },
      { path: 'userList', component: UserDetailsComponent },
      { path: 'states', component: StatesComponent },
      { path: 'seviceForm', component: FormComponent },
      { path: '**', component: ErrorComponent },
    ]
  },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
