import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  users: any = {
    "admin": {
      name: "Admin User",
      username: "admin",
      password: "admin",
      role: "admin"
    },
    "sales": {
      name: "Sales User",
      username: "sales",
      password: "sales",
      role: "sales"
    },
    "staff": {
      name: "Staff User",
      username: "staff",
      password: "staff",
      role: "staff"
    }
  };
  
  login(username: any, password: any): any | void {
    console.log(this.users)
    if (this.users.hasOwnProperty(username)) {
      const user: any = this.users[username];
      if (user.password == password) {
        sessionStorage.setItem('currentUser', this.users[username]);
        return true;
      }

    }
    return false;
  }
  logout(): void {
    sessionStorage.removeItem('currentUser');
  }
  public get loggedIn(): boolean {
    return (sessionStorage.getItem('currentUser') !== null);
  }

  public getLoggedInUser() {
    return sessionStorage.getItem('currentUser');
  }
}
