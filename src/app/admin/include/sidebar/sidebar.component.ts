import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor() { }
  User: any
  userSession:any;
  ngOnInit(): void {
    this.userSession = sessionStorage.getItem("currentUser")
    console.log(this.userSession);
  }

  isAdmin() {
    if (this.User == "admin") {
      return true;
    } else {
      return false
    }
  }

  isStaff(){
    if (this.User == "staff") {
      return true;
    } else {
      return false
    }
  }

}
