import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/authentication.service';

@Component({
  selector: 'app-admin-header',
  templateUrl: './admin-header.component.html',
  styleUrls: ['./admin-header.component.css']
})
export class AdminHeaderComponent implements OnInit {
  constructor(private router: Router, private authenticationService: AuthenticationService) { } 

  ngOnInit(): void {
  }
  logout() {  
    this.authenticationService.logout();  
    this.router.navigate(['login']);  
  
}

}
