import { HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, retry, throwError } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class GetCountService {
//  private url = 'https://staging.parivahan.gov.in/admin-stg/api/dashboard/get-echallan-count';
//   constructor (private http:HttpClient) {}

//   getEchallanCount()
//   {
//     return this.http.get(this.url); 
//   }

constructor(private http : HttpClient){}
  

  GetData(url:string):Observable<any>
  {
return this.http.get(url).pipe(

  catchError(this.handleError)
)
  };


// **************post ***********

PostData(url:string ,body:any):Observable<any>
{
return this.http.post(url,body).pipe(
  
  retry(2),

catchError(this.handleError)
)
};



  public handleError(err:HttpErrorResponse)
  {
    return throwError(err.status)
  };
}



